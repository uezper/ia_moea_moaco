def main():
    import sys
    import os.path

    sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
    runAlgorithms()

def calculateMetrics():
    from src.metrics import m1, m2, m3, m4

    f = open('results/metrics.txt', 'w+')
    for i in range(1,3):
        moacs = readFront('results/moacs_{0}.txt'.format(i))
        spea = readFront('results/spea_{0}.txt'.format(i))
        ytrue = readFront('results/ytrue_{0}.txt'.format(i))

        f.write('moacs_{0}:\n'.format(i))
        f.write('m1={0}\n'.format(m1(moacs, ytrue)))
        f.write('m2={0}\n'.format(m2(moacs, 50)))
        f.write('m3={0}\n'.format(m3(moacs)))
        f.write('m4={0}\n'.format(m4(moacs, ytrue)))
        f.write('spea_{0}:\n'.format(i))
        f.write('m1={0}\n'.format(m1(spea, ytrue)))
        f.write('m2={0}\n'.format(m2(spea, 50)))
        f.write('m3={0}\n'.format(m3(spea)))
        f.write('m4={0}\n'.format(m4(spea, ytrue)))

    f.close()




def readFront(file):
    f = open(file, 'r')
    r = f.readlines()
    frente = []
    for i in range(0, len(r)):
        l = r[i]
        if l[0] == 'f' and l[1] == '1':
            f1 = l.split('[')[1].split(']')[0].split(',')
            f2 = r[i+1].split('[')[1].split(']')[0].split(',')

            f1 = [num(x) for x in f1]
            f2 = [num(x) for x in f2]

            frente = frente + [(x,y) for x,y in zip(f1,f2)]

    frente = noDominated(frente)
    print(frente)
    f.close()
    return frente


def num(s):
    try:
        return int(s)
    except ValueError:
        return float(s)

def runAlgorithms():
    files = ['problems/vrptw_rc101.txt', 'problems/vrptw_c101.txt']

    for instancia in range(0, 2):

        ytrue = []
        print('Corriendo SPEA - Instancia {0}'.format(instancia+1))
        ytrue = ytrue + VrptwEA(files[instancia], instancia+1)
        print('Corriendo MOACS - Instancia {0}'.format(instancia+1))
        ytrue = ytrue + VrptwACO(files[instancia], instancia+1)
        ytrue = noDominated(ytrue)

        f = open('results/ytrue_{0}.txt'.format(instancia+1), 'w+')
        f1 = []
        f2 = []
        for sol in ytrue:
           f1.append(sol[0])
           f2.append(sol[1])

        f.write('{0}={1};\n'.format('f1', f1))
        f.write('{0}={1};\n'.format('f2', f2))
        f.close()

    print('Calculando métricas')
    calculateMetrics()
    print('Finalizado')


def noDominated(sol):
    r = []
    for x in sol:
        dominado = False
        for y in sol:
            if x!=y and x and covers(y,x):
                dominado = True
                break
        if not dominado and x not in r: r.append(x)

    return r

def covers(s1, s2):
        return s1[0] <= s2[0] and s1[1] <= s2[1] and (s1[0] < s2[0] or s1[1] < s2[1])

def VrptwACO(file, output):
    from src.VrptwMoacs import VrptwMoacs
    from src.ProblemLoader import ProblemLoader

    r = []
    f = open('results/moacs_{0}.txt'.format(output), 'w+')

    p = ProblemLoader(file)

    params_gen = [50, 100, 150, 200, 250]
    params_pop = [60, 50, 40, 30, 20]


    for it in range(0, 5):
        print('Iteracion {0}'.format(it+1))
        f.write('===================== ITERACION {0} ===========================\n'.format(it+1))

        options = {
            'matrix': p.matrix,
            'demand': p.q,
            'ready': p.b,
            'done': p.e,
            'service': p.s,
            'maxCapacity': p.Q,
            'explotation': 0,
            'beta': 0.3,
            'maxIterations': params_gen[it],
            'antSize': params_pop[it],
            'p': 0.7,
        }


        x = VrptwMoacs(**options)
        b = x.run()
        i = 1
        f1 = []
        f2 = []
        for sol in b:
            f.write('--------------------' + '\n')
            f.write('Solucion ' + str(i) + '\n')
            f.write('Camino ' + str(sol.path) + '\n')
            f.write('Vehiculos ' + str(sol.v) + '\n')
            f.write('Tiempo total ' + str(sol.t) + '\n')
            r.append((sol.v, sol.t))
            f1.append(sol.v)
            f2.append(sol.t)
            i = i + 1

        f.write('{0}={1};\n'.format('f1', f1))
        f.write('{0}={1};\n'.format('f2', f2))
        f.flush()

    f.close()
    return r

def VrptwEA(file, output):
    from src.ProblemLoader import ProblemLoader
    from src.VrptwSpea import VrptwSpea

    r = []
    f = open('results/spea_{0}.txt'.format(output), 'w+')

    params_gen = [300, 350, 400, 450, 500]
    params_pop = [200, 150, 100, 50, 20]


    p = ProblemLoader(file)



    for it in range(0, 5):
        print('Iteracion {0}'.format(it+1))
        f.write('===================== ITERACION {0} ===========================\n'.format(it+1))

        options = {
            'populationSize': params_pop[it],
            'maxGeneration': params_gen[it],
            'maxParetoPoints': 100,
            'maxFixed': 50000,
            'mutationRate': 0.4,
            'matrix': p.matrix,
            'demand': p.q,
            'ready': p.b,
            'done': p.e,
            'service': p.s,
            'maxCapacity': p.Q,
    }


        x = VrptwSpea(**options)
        b = x.run()


        i = 1
        f1 = []
        f2 = []
        for sol in b:
            f.write('--------------------\n')
            f.write('Solucion ' + str(i) + '\n')
            f.write('Camino ' + str(sol.genoma) + '\n')
            f.write('Vehiculos ' + str(sol.fenoma.v) + '\n')
            f.write('Tiempo total ' + str(sol.fenoma.t) + '\n')
            r.append((sol.fenoma.v, sol.fenoma.t))

            f1.append(sol.fenoma.v)
            f2.append(sol.fenoma.t)
            i = i + 1

        f.write('{0}={1};\n'.format('f1', f1))
        f.write('{0}={1};\n'.format('f2', f2))
        f.flush()

    f.close()
    return r

if __name__ == "__main__":
    main()