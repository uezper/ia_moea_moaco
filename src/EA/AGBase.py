import math

class AGBase:
    def __init__(self, **kwargs):
        self.populationSize = kwargs.get('populationSize', 20)
        self.mutationRate = kwargs.get('mutationRate', 0.01)
        self.elitismRate = kwargs.get('elitismRate', 0.0)
        self.maxGeneration = kwargs.get('maxGeneration', 0)
        self.maxError = kwargs.get('maxError', 0.00001)
        self.maxFixed = kwargs.get('maxFixed', 5)

    def __initValues(self):
        self.gen = 0
        self.error = 99999
        self.fixed = 0
        self.lastBest = None
        self.best = None
        self.population = None

    def __stopReached(self):
        return ((self.maxGeneration > 0 and self.maxGeneration <= self.gen)
        or (self.error < self.maxError)
        or (self.fixed > self.maxFixed))

    def __applyOperators(self):

        newpopulation = []

        elitism = self.elitismSelection(math.ceil(self.elitismRate * self.populationSize))
        crossover = self.selection(self.population)

        newpopulation = newpopulation + elitism



        while(len(crossover) > 1 and len(newpopulation) < self.populationSize):
            indv1 = crossover.pop(0)
            indv2 = crossover.pop(0)

            [newindv1, newindv2] = self.crossOver(indv1, indv2)

            newpopulation.append(newindv1)

            if (len(newpopulation) < self.populationSize):
                newpopulation.append(newindv2)



        if (len(newpopulation) < self.populationSize):
            raise Exception('La poblacion siguiente no tiene el size requerido:' +
                            str(len(newpopulation)) + '/' + str(self.populationSize))

        self.population = self.mutation(newpopulation, self.mutationRate)


    def mutation(self, population, mutationRate):
        raise Exception('Unimplemented method')

    def selection(self, population):
        raise Exception('Unimplemented method')

    def crossOver(self, indv1, indv2):
        raise Exception('Unimplemented method')

    def elitismSelection(self, size):
        raise Exception('Unimplemented method')

    def fitnessAssigment(self, population):
        raise Exception('Unimplemented method')

    def initialPopulation(self, populationSize):
        raise Exception('Unimplemented method')

    def updateBest(self, population):
        raise Exception('Unimplemented method')

    def compareBest(self, best, lastBest):
        raise Exception('Unimplemented method')

    def run(self):

        self.__initValues()
        self.population = self.initialPopulation(self.populationSize)




        if (len(self.population) != self.populationSize):
            raise Exception('La poblacion inicial no tiene el size requerido:' +
                            str(len(self.population)) + '/' + str(self.populationSize))

        while (not self.__stopReached()):


            self.fitnessAssigment(self.population)

            self.lastBest = self.best
            self.best = self.updateBest(self.population)


            if (self.compareBest(self.best,self.lastBest)):
                self.fixed = self.fixed + 1
            else:
                self.fixed = 0


            self.__applyOperators()

            self.gen = self.gen + 1


            #print('----------------------------------')
            #print('Gen:' + gen)
            #print([(p.fitness, p.fenoma) for p in self.best])
            #print([(p.fitness, p.fenoma) for p in self.population])
            #print('----------------------------------')

        return self.best




