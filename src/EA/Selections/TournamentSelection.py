class TournamentSelection:
    def __init__(self, k):
        self.k = k


    def selection(self, population):
        list = []
        for i in range(0, len(population)):
            list.append(self.selectOne(population))

        return list

    def selectOne(self, population):
        import random
        n = len(population)
        best = None

        for i in range(0, self.k):
            ind = population[random.randint(0, n-1)]
            if (best == None or ind.fitness > best.fitness):
                best = ind

        return best