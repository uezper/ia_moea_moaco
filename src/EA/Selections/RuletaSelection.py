class RuletaSelection:

    def __init__(self, isMax):
        self.isMax = isMax

    def selection(self, population):
        import math
        
        population = population
        eval = []

        sum = 0
        for i in range(0, len(population)):
            sum += population[i].fitness

        for i in range(0, len(population)):
            if (self.isMax):
                if (sum == 0):
                    eval.append(0)
                else:
                    eval.append(population[i].fitness / sum)
            else:
                if (population[i].fitness == 0):
                    eval.append(1)
                else:
                    eval.append(sum / population[i].fitness)

        z = [(c,w) for (c,w) in zip(population, eval)]
        r = self.__g(z, math.floor(len(population) / 2))
        return r

    def __g(self, choices, n):
        r = []
        for i in range(0,n):
            one = self.__f(choices)
            r.append(choices[one])
            choices.pop(one)
            two = self.__f(choices)
            r.append(choices[two])
            choices.pop(two)
        r = [x for (x,y) in r]
        return r

    def __f(self, choices):
        from random import random
        from bisect import bisect

        values, weights = zip(*choices)
        total = 0
        cum_weights = []
        for w in weights:
            total += w
            cum_weights.append(total)

        x = random() * total
        i = bisect(cum_weights, x)
        return i