from src.EA.AGBase import AGBase
from src.EA.Selections.TournamentSelection import TournamentSelection

class SpeaBase(AGBase):

    def __init__(self, **kwargs):
        super(SpeaBase, self).__init__(**kwargs)
        self.maxParetoPoints = kwargs.get('maxParetoPoints', 10)
        self.elitismRate = 0
        self.tournament = TournamentSelection(2)

    def selection(self, population):
        return self.tournament.selection(self.population)

    def fitnessAssigment(self, population):

        # Update external Pareto set
        if (self.best == None): self.best = []



        A = self.collectNonDominatedSolutions(population)
        B = self.__combineParetoSet(self.best, A)


        if (len(B) > self.maxParetoPoints):
            self.best = self.__reduceParetoSet(B)
        else:
            self.best = B

        # Calculate Pareto Strengths
        for paretoInd in self.best:
            count = 0
            for popInd in population:
                if self.covers(paretoInd, popInd):
                    count = count + 1
            strength = count / (len(population) + 1)
            paretoInd.fitness = strength

        # Determine fitness values
        # El fitness se pone invertido por cuestiones de seleccion
        for popInd in population:
            sum = 0
            for paretoInd in self.best:
                if self.covers(paretoInd, popInd):
                    sum = sum + paretoInd.fitness
            popInd.fitness = -(sum + 1)





    def updateBest(self, population):
        return self.best

    def compareBest(self, best, lastBest):
        if len(best) != len(lastBest): return False

        best.sort(key=lambda ind: ind.fitness)
        lastBest.sort(key=lambda ind: ind.fitness)

        for x,y in zip(best, lastBest):
            if (x.fitness != y.fitness): return True
        return True


    def __combineParetoSet(self, paretoSet, set):

        paretoSet = paretoSet + set
        newParetoSet = self.collectNonDominatedSolutions(paretoSet)

        return newParetoSet

    def collectNonDominatedSolutions(self, population):
        nonDominated = []

        for x in population:
            dominated = False
            for y in population:
                if (x != y and self.covers(y, x)):
                    dominated = True
                    break

            if not dominated:
                nonDominated.append(x)

        return nonDominated


    def __reduceParetoSet(self, paretoSet):
        import itertools
        clusterSet = []

        # Each Pareto point forms a cluster
        for paretoInd in paretoSet:
            clusterSet.append(Cluster([paretoInd]))

        # Join clusters until number of clusters remains under maximum
        while (len(clusterSet) > self.maxParetoPoints):
            # Select two clusters which have minimum distance
            minDistance = float('inf')
            cluster1 = None
            cluster2 = None
            for x,y in itertools.combinations(clusterSet, 2):
                d = Cluster.clusterDistance(x,y, self.distanceFunction)
                if (d < minDistance):
                    cluster1 = x
                    cluster2 = y
                    minDistance = d

            # Join the selected clusters
            newCluster = Cluster(cluster1.members + cluster2.members)
            clusterSet.remove(cluster1)
            clusterSet.remove(cluster2)
            clusterSet.append(newCluster)



        # For each cluster pick out representative solution (centroid)
        paretoSet = []
        for cluster in clusterSet:
            paretoInd = cluster.getCentroid(self.distanceFunction)
            paretoSet.append(paretoInd)

        return paretoSet

    def elitismSelection(self, size):
        return []


    def covers(self, a, b):
        raise Exception('Unimplemented method')

    def crossOver(self, indv1, indv2):
        raise Exception('Unimplemented method')

    def mutation(self, population, mutationRate):
        raise Exception('Unimplemented method')

    def initialPopulation(self, populationSize):
        raise Exception('Unimplemented method')

    def distanceFunction(self, ind1, ind2):
        raise Exception('Unimplemented method')

class Cluster:
    def __init__(self, members):
        self.members = members

    def getCentroid(self, distanceFunction):
        m_copy = self.members
        centroid = None
        centroid_distance = 0

        for x in self.members:
            distance = 0
            for y in m_copy:
                if (x != y):
                    distance = distance + distanceFunction(x,y)

            if (centroid == None or distance < centroid_distance):
                centroid = x
                centroid_distance = distance

        return centroid

    @staticmethod
    def clusterDistance(cluster1, cluster2, distanceFunction):
        import itertools
        list_ = itertools.product(cluster1.members, cluster2.members)
        distance = 0
        for x,y in list_:
            distance = distance + distanceFunction(x,y)

        if (distance > 0): distance = distance / (len(cluster1.members) * len(cluster2.members))
        return distance