from src.MOACS.Moacs import Moacs

class VrptwMoacs(Moacs):
    def __init__(self, **kwargs):
        super(VrptwMoacs, self).__init__(**kwargs)
        self.matrix = kwargs.get('matrix', None)
        self.q = kwargs.get('demand', None)
        self.b = kwargs.get('ready', None)
        self.e = kwargs.get('done', None)
        self.s = kwargs.get('service', None)
        self.Q = kwargs.get('maxCapacity', None)
        self.q0 = kwargs.get('explotation', 0)
        self.beta = kwargs.get('beta', 0.3)
        self.maxIterations = kwargs.get('maxIterations', 100)


        if (self.matrix == None):
            raise Exception('Matrix information missing.')

        if (self.q == None):
            raise Exception('Demand information missing.')

        if (self.Q == None):
            raise Exception('Max capacity information missing.')

        if (self.b == None):
            raise Exception('Ready information missing.')

        if (self.e == None):
            raise Exception('Done information missing.')

        if (self.s == None):
            raise Exception('Service information missing.')


    def initialSolution(self):
        from random import random
        self.t0 = random()
        self.tau = self.initializeTau()
        sol = self.build_sol(round(random() * self.antSize))

        return sol

    def initializeTau(self):
        n = len(self.matrix[1])
        t = [[self.t0 for x in range(0, n)] for y in range(0, n)]

        return t


    def getPairIJ(self, sol):
        r = []
        for p in sol.path:
            i = 0
            for j in p:
                r.append((i, j))
                i = j
            r.append((i, 0))
        return r


    def setTau(self, i, j, val):
        self.tau[i][j] = val

    def getTau(self, i, j):
        return self.tau[i][j]

    def update_pareto(self, sol):
        p = self.P + [sol]
        newP = []
        newP_set = []

        for x in p:
            dominated = False

            for y in p:
                if x!=y and self.covers(y, x):
                    dominated = True
                    break
                else:
                    for s in newP:
                        if self.equals(s, x):
                            dominated = True
                            break

            if not dominated and not (x.v, x.t) in newP_set:
                newP.append(x)
                newP_set.append((x.v, x.t))

        return newP

    def equals(self, s1, s2):
        return s1.path == s2.path

    def covers(self, s1, s2):
        return s1.v <= s2.v and s1.t <= s2.t and (s1.v < s2.v or s1.t < s2.t)

    def build_sol(self, k):
        i = 0
        sol = Solution()
        p = []
        t = 0
        q = 0
        visited = []
        added = 0
        n = len(self.matrix[0]) - 1
        v = 1

        while added < n:

            N = self.computeN(i, q, p, visited)


            if len(N) == 0:
                sol.path.append(p)
                added = added + len(p)
                t = t + self.matrix[i][0]
                q = 0
                i = 0
                v = v + 1
                p = []
            else:
                vf1 = []
                vf2 = []
                for j in N:
                    vf1.append(1/v)
                    vf2.append(1/self.matrix[i][j])

                j = self.chooseNextNode(k, i, N, vf1, vf2)
                visited.append(j)
                p.append(j)
                t = t + self.matrix[i][j] + self.s[j]
                q = q + self.q[j]
                val = (1 - self.p) * self.getTau(i, j) + self.p * self.t0
                self.setTau(i, j, val)
                i = j


        t = t + self.matrix[i][0]
        sol.v = v - 1
        sol.t = t
        return sol


    def chooseNextNode(self, k, i, N, vf1, vf2):
        from random import random

        p = []
        l = k / self.antSize

        sum = 0

        for j, v1, v2 in zip(N, vf1, vf2):
            v = (self.getTau(i, j) * (v1**(l * self.beta) * (v2**((1-l)*self.beta))))
            sum = sum + v
            p.append(v)

        for i in range(0, len(p)):
            p[i] = p[i] / sum


        if (random() < self.q0):
            maxi = None
            maxv = None

            for i in range(0, len(p)):
                if maxv == None or p[i] > maxv:
                    maxv = p[i]
                    maxi = i

            return N[maxi]
        else:
            i = self.__f(zip(N, p))
            return N[i]


    def __f(self, choices):
        from random import random
        from bisect import bisect

        values, weights = zip(*choices)
        total = 0
        cum_weights = []
        for w in weights:
            total += w
            cum_weights.append(total)

        x = random() * total
        i = bisect(cum_weights, x)
        return i

    def computeN(self, i, load, p, visited):
        N = []
        n = len(self.matrix[0])

        for x in range(1, n):
            if x != i and not x in visited and load + self.q[x] <= self.Q:
                continue_ = True
                p.append(x)

                sum = 0
                for k in range(1, len(p)):
                    sum = self.matrix[0][p[0]]
                    for j in range(0, k):
                        sum = sum + self.matrix[p[j]][p[j+1]] + self.s[p[j]]

                    continue_ = continue_ and self.b[p[k]] <= sum and sum <= self.e[p[k]] - self.s[p[k]]

                continue_ = continue_ and (sum + self.s[x] + self.matrix[0][x]) <= self.e[0]

                p.remove(x)

                if (continue_ or len(p) == 0):
                    N.append(x)

        return N


    def stopReached(self):
        return self.i > self.maxIterations

    def f1(self, sol):
        return sol.v

    def f2(self, sol):
        return sol.t / sol.v

class Solution:
    def __init__(self):
        self.path = []
        self.v = 0
        self.t = 0