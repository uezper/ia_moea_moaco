from src.EA.AGBase import AGBase
from src.EA.Selections.RuletaSelection import RuletaSelection
from src.EA.Individual import Individual

class AGMax(AGBase):

    def __init__(self, **kwargs):
        super(AGMax, self).__init__(**kwargs)
        self.select = RuletaSelection(False)

    def selection(self, population):
        return self.select.selection(self.population)

    def crossOver(self, indv1, indv2):
        from random import random


        newindv1 = Individual()
        newindv1.genoma = []

        newindv2 = Individual()
        newindv2.genoma = []

        for i in range(0, len(indv1.genoma)):
            if random() >= 0.5:
                newindv1.genoma.append(indv1.genoma[i])
                newindv2.genoma.append(indv2.genoma[i])
            else:
                newindv2.genoma.append(indv1.genoma[i])
                newindv1.genoma.append(indv2.genoma[i])


        newindv1.fenoma = int(''.join(str(e) for e in newindv1.genoma),2)
        newindv2.fenoma = int(''.join(str(e) for e in newindv2.genoma),2)

        return [newindv1, newindv2]


    def fitnessAssigment(self, population):
        for p in population:
            p.fitness = abs(p.fenoma * 7 - 28)

    def initialPopulation(self, populationSize):
        from random import randint

        pop = []
        for i in range(0, populationSize):
            x = Individual()
            x.fenoma = randint(1, 15)
            x.genoma =  [int(x) for x in bin(x.fenoma)[2:]]
            x.genoma = (4 - len(x.genoma)) * [0] + x.genoma
            pop.append(x)

        return pop

    def updateBest(self, population):
        r = []
        if (self.best != None): r = [(x.fitness, x) for x in self.best]
        r = r + [(x.fitness, x) for x in population]
        r.sort(key=lambda pair: pair[0])
        return r[0][1]

    def compareBest(self, best, lastBest):
        if (lastBest == None): return False
        return (best[0].fitness == lastBest[0].fitness)

