def pruebaMoacs():
    from src.VrptwMoacs import VrptwMoacs

    r = []
    f = open('results/example.txt', 'w+')


    for it in range(0, 1):
        print('Iteracion {0}'.format(it+1))
        f.write('===================== ITERACION {0} ===========================\n'.format(it+1))

        options = {
            'matrix': [[0,2,3,2,5,1],
                   [2,0,2,5,2,3],
                   [3,2,0,3,7,4],
                   [2,5,3,0,7,5],
                   [5,2,7,7,0,1],
                   [1,3,4,5,1,0]],
            'demand':[0,1,1,1,1,1],
            'ready': [0,7,5,1,3,2],
            'done': [20,20,8,9,12,5],
            'service': [0,5,3,4,7,2],
            'maxCapacity': 2,
            'explotation': 0,
            'beta': 0.3,
            'maxIterations': 100,
            'antSize': 14,
            'p': 0.7,
        }


        x = VrptwMoacs(**options)
        b = x.run()
        i = 1
        f1 = []
        f2 = []
        for sol in b:
            f.write('--------------------' + '\n')
            f.write('Solucion ' + str(i) + '\n')
            f.write('Camino ' + str(sol.path) + '\n')
            f.write('Vehiculos ' + str(sol.v) + '\n')
            f.write('Tiempo total ' + str(sol.t) + '\n')
            r.append((sol.v, sol.t))
            f1.append(sol.v)
            f2.append(sol.t)
            i = i + 1

        f.write('{0}={1};\n'.format('f1', f1))
        f.write('{0}={1};\n'.format('f2', f2))
        f.flush()

    f.close()

def pruebaEa():
    from src.VrptwSpea import VrptwSpea

    r = []
    f = open('results/example.txt', 'w+')

    for it in range(0, 1):
        print('Iteracion {0}'.format(it+1))
        f.write('===================== ITERACION {0} ===========================\n'.format(it+1))

        options = {
            'populationSize': 14,
            'maxGeneration': 100,
            'maxParetoPoints': 100,
            'maxFixed': 50000,
            'mutationRate': 0.4,
            'matrix': [[0,2,3,2,5,1],
                       [2,0,2,5,2,3],
                       [3,2,0,3,7,4],
                       [2,5,3,0,7,5],
                       [5,2,7,7,0,1],
                       [1,3,4,5,1,0]],
            'demand':[0,1,1,1,1,1],
            'ready': [0,7,5,1,3,2],
            'done': [20,20,8,9,12,5],
            'service': [0,5,3,4,7,2],
            'maxCapacity': 5,
        }


        x = VrptwSpea(**options)
        b = x.run()


        i = 1
        f1 = []
        f2 = []
        for sol in b:
            f.write('--------------------\n')
            f.write('Solucion ' + str(i) + '\n')
            f.write('Camino ' + str(sol.genoma) + '\n')
            f.write('Vehiculos ' + str(sol.fenoma.v) + '\n')
            f.write('Tiempo total ' + str(sol.fenoma.t) + '\n')
            r.append((sol.fenoma.v, sol.fenoma.t))

            f1.append(sol.fenoma.v)
            f2.append(sol.fenoma.t)
            i = i + 1

        f.write('{0}={1};\n'.format('f1', f1))
        f.write('{0}={1};\n'.format('f2', f2))
        f.flush()

    f.close()

