from src.EA.Individual import Individual
from src.EA.MOEA.SpeaBase import SpeaBase


class ShafferTest(SpeaBase):

    def __init__(self, **kwargs):
        super(ShafferTest, self).__init__(**kwargs)
        self.minVal = -6
        self.maxVal = 6
        self.bits = 14

    def initialPopulation(self, populationSize):
        from random import randint
        pop = []
        max_num = 2**self.bits - 1

        for i in range(0, populationSize):
            x = Individual()
            x.genoma = [str(i) for i in bin(randint(0, max_num))[2:]]
            x.genoma = (self.bits - len(x.genoma)) * [0] + x.genoma
            x.fenoma = self.genToFen(x.genoma)

            pop.append(x)

        return pop

    def genToFen(self, genoma):
        i = int(''.join(str(x) for x in genoma),2)
        return self.minVal + i * (abs(self.maxVal - self.minVal)/(2**self.bits - 1))

    def f1(self, x):
        return x**2

    def f2(self, x):
        return (x-2)**2

    def covers(self, a, b):
        a = a.fenoma
        b = b.fenoma
        return self.f1(a) <= self.f1(b) and self.f2(a) <= self.f2(b) and (self.f1(a) < self.f1(b) or self.f2(a) < self.f2(b))

    def crossOver(self, indv1, indv2):
        from random import random

        newindv1 = Individual()
        newindv1.genoma = []

        newindv2 = Individual()
        newindv2.genoma = []

        for i in range(0, len(indv1.genoma)):
            if random() >= 0.5:
                newindv1.genoma.append(indv1.genoma[i])
                newindv2.genoma.append(indv2.genoma[i])
            else:
                newindv2.genoma.append(indv1.genoma[i])
                newindv1.genoma.append(indv2.genoma[i])


        newindv1.fenoma = self.genToFen(newindv1.genoma)
        newindv2.fenoma = self.genToFen(newindv2.genoma)

        return [newindv1, newindv2]

    def mutation(self, population, mutationRate):
        return population

    def distanceFunction(self, ind1, ind2):
        x1, y1 = self.f1(ind1.fenoma), self.f2(ind1.fenoma)
        x2, y2 = self.f1(ind2.fenoma), self.f2(ind2.fenoma)

        return ((x1 - x2)**2 + (y1 - y2)**2)**(1/2)