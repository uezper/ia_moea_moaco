def m1(front, refFront):

    s = 0

    for p in front:
        min_d = None
        for q in refFront:
            d = distance(p, q)
            if min_d == None or d < min_d:
                min_d = d
        s = s + min_d

    return s / len(front)


def m2(front, sigma):

    s = 0
    for p in front:
        set = []
        for q in front:
            if distance(p, q) < sigma:
                set.append(q)
        s = s + len(set)

    if (len(front) > 1):
        return s / (len(front) - 1)

    return s

def m3(front):

    s = 0
    for i in range(0, 2):
        max = None
        for p in front:
            for q in front:
                d = abs(p[i] - q[i])
                if max == None or d > max:
                    max = d
        s = s + max

    return s**0.5

def m4(front, refFront):
    return 1 - (len(set(front) & set(refFront)) / len(front))


def distance(s1, s2):
    return ((s1[0] - s2[0])**2 + (s1[1] - s2[1])**2)**0.5