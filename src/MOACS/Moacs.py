class Moacs:
    def __init__(self, **kwargs):
        self.antSize = kwargs.get('antSize', 10)
        self.p = kwargs.get('p', 0.3)

    def run(self):
        self.P = []
        sol_0 = self.initialSolution()
        self.t0 = self.calculatet0([sol_0])
        self.tau = self.initializeTau()
        self.i = 0

        while not self.stopReached():
            #print('-------------------------')
            #print('Iteracion: ' + str(self.i))
            for k in range(0, self.antSize):
                #print('Hormiga: ' + str(k))
                sol_k = self.build_sol(k)
                #print('Solucion: ' + str(sol_k.path) + ' <> ' + str(sol_k.v) + ' <> ' + str(sol_k.t))
                self.P = self.update_pareto(sol_k)

            t0_ = self.calculatet0(self.P)
            if t0_ > self.t0:
                self.t0 = t0_
                self.tau = self.initializeTau()
            else:
                for sol in self.P:
                    L = self.f1(sol)
                    J = self.f2(sol)
                    for i,j in self.getPairIJ(sol):
                        v = (1 - self.p) * self.getTau(i,j) + (self.p / (L * J))
                        self.setTau(i,j, v)

            self.i = self.i + 1

        return self.P

    def calculatet0(self, P):
        f1_mean = 0
        f2_mean = 0

        for sol in P:
            f1_mean = f1_mean + self.f1(sol)
            f2_mean = f2_mean + self.f2(sol)

        f1_mean = f1_mean / len(P)
        f2_mean = f2_mean / len(P)

        return 1/(f1_mean * f2_mean)

    def initialSolution(self):
        raise Exception('Unimplemented method.')

    def initializeTau(self):
        raise Exception('Unimplemented method.')

    def getPairIJ(self, sol):
        raise Exception('Unimplemented method.')

    def setTau(self, i, j, val):
        raise Exception('Unimplemented method.')

    def getTau(self, i, j):
        raise Exception('Unimplemented method.')

    def update_pareto(self, sol):
        raise Exception('Unimplemented method.')

    def build_sol(self, k):
        raise Exception('Unimplemented method.')

    def stopReached(self):
        raise Exception('Unimplemented method.')

    def f1(self, sol):
        raise Exception('Unimplemented method.')

    def f2(self, sol):
        raise Exception('Unimplemented method.')