from src.EA.Individual import Individual
from src.EA.MOEA.SpeaBase import SpeaBase


class VrptwSpea(SpeaBase):

    def __init__(self, **kwargs):
        super(VrptwSpea, self).__init__(**kwargs)
        self.matrix = kwargs.get('matrix', None)
        self.q = kwargs.get('demand', None)
        self.b = kwargs.get('ready', None)
        self.e = kwargs.get('done', None)
        self.s = kwargs.get('service', None)
        self.Q = kwargs.get('maxCapacity', None)

        if (self.matrix == None):
            raise Exception('Matrix information missing.')

        if (self.q == None):
            raise Exception('Demand information missing.')

        if (self.Q == None):
            raise Exception('Max capacity information missing.')

        if (self.b == None):
            raise Exception('Ready information missing.')

        if (self.e == None):
            raise Exception('Done information missing.')

        if (self.s == None):
            raise Exception('Service information missing.')

    def covers(self, a, b):
        return (a.fenoma.v <= b.fenoma.v and a.fenoma.t <= b.fenoma.t and (a.fenoma.v < b.fenoma.v or a.fenoma.t < b.fenoma.t))


    def initialPopulation(self, populationSize):
        from random import random

        p = []
        for i in range(0, populationSize):
            ind = Individual()

            clientes = [x for x in range(1, len(self.matrix[0]))]

            path = []
            while len(clientes) > 0:
                x = round(random() * (len(clientes) - 1))
                path.append(clientes[x])
                clientes.remove(clientes[x])

            ind.genoma = path
            ind = self.repair(ind)
            p.append(ind)


        return p

    def distanceFunction(self, ind1, ind2):
        return ((ind1.fenoma.v - ind2.fenoma.v)**2 + (ind1.fenoma.t - ind2.fenoma.t)**2)**0.5


    def crossOver(self, indv1, indv2):


        from random import random

        newindv1 = Individual()
        newindv1.genoma = []

        newindv2 = Individual()
        newindv2.genoma = []


        t1 = [item for sublist in indv1.genoma for item in sublist]
        t2 = [item for sublist in indv2.genoma for item in sublist]

        r = round(random() * (len(t1) - 1))

        newindv1.genoma = t1[0:r] + t2[r:]
        newindv2.genoma = t2[0:r] + t1[r:]


        newindv1 = self.repair(newindv1)
        newindv2 = self.repair(newindv2)


        return [newindv1, newindv2]

    def mutation(self, population, mutationRate):
        from random import random, shuffle, randint

        for i in range(0, len(population)):
            if random() <= mutationRate:
                ind = population[i]

                t1 = [item for sublist in ind.genoma for item in sublist]

                r1 = randint(0, len(t1) - 1)
                r2 = r1
                while (r2 == r1): r2 = randint(0, len(t1) - 1)

                f = min(r1,r2)
                t = max(r1,r2)


                t_ = t1[f:t]
                shuffle(t_)


                t1[f:t] = t_

                ind.genoma = t1
                ind = self.repair(ind)
                population[i] = ind

        return population


    def repair(self, ind):

        from random import shuffle

        # Verificar que sea una permutacion de elementos
        # los elementos restantes son agregados al final

        t1 = [x for x in ind.genoma]

        restantes = [x for x in range(1,len(self.matrix[0]))]
        newind = []

        for x in t1:
            if x in restantes:
                newind.append(x)
                restantes.remove(x)

        shuffle(restantes)
        for x in restantes:
            newind.append(x)

        newl = []
        for e in newind:

            # Verificacion de restricciones 1 y 2
            if len(newl) > 0:
                newl[-1].append(e)
                if not (self.verifyRestriction1(newl[-1]) and self.verifyRestriction2(newl[-1])):
                    newl[-1].remove(e)
                    newl.append([e])
            else:
                newl.append([e])


        t = self.calculateTime(newl)

        newind = Individual()
        newind.genoma = newl
        newind.fenoma = Solution()
        newind.fenoma.v = len(newl)
        newind.fenoma.t = t


        return newind

    def calculateTime(self, path):
        t = 0
        for recorrido in path:
            t = t + self.matrix[0][recorrido[0]] + self.s[recorrido[-1]] + self.matrix[0][recorrido[-1]]
            t = t + sum([ self.matrix[recorrido[j]][recorrido[j+1]] + self.s[recorrido[j]] for j in range(0, len(recorrido) - 1)])
        return t


    def verifyRestriction1(self, recorrido):

        load = sum([self.q[x] for x in recorrido])
        return load <= self.Q

    def verifyRestriction2(self, recorrido):

        continue_ = True
        sum = 0
        for k in range(0, len(recorrido)):

            sum = self.matrix[0][recorrido[0]]
            for j in range(0, k):
                sum = sum + self.matrix[recorrido[j]][recorrido[j+1]] + self.s[recorrido[j]]

            if (k > 0): continue_ = continue_ and self.b[recorrido[k]] <= sum and sum <= self.e[recorrido[k]] - self.s[recorrido[k]]

        continue_ = continue_ and (sum + self.s[recorrido[-1]] + self.matrix[0][recorrido[-1]] <= self.e[0])

        return continue_

    def collectNonDominatedSolutions(self, population):
        nonDominated = []
        nonDominated_set = []


        for x in population:
            dominated = False
            for y in population:
                if (x != y and self.covers(y, x)):
                    dominated = True
                    break

            if not dominated and (x.fenoma.v, x.fenoma.t) not in nonDominated_set:
                nonDominated.append(x)
                nonDominated_set.append((x.fenoma.v, x.fenoma.t))



        return nonDominated


class Solution:
    def __init__(self):
        self.path = []
        self.v = 0
        self.t = 0