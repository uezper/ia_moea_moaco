class ProblemLoader:
    def __init__(self, file):

        f = open(file, 'r')
        r = f.readlines()
        f.close()

        N = int(r[1])
        self.Q = int(r[3])
        self.q = []
        self.b = []
        self.e = []
        self.s = []
        self.matrix = [[0 for x in range(0, N)] for y in range(0, N)]

        coords = []

        for i in range(5, 5 + N):
            l = [num(s) for s in r[i].split() if isNumber(s)]
            coords.append( (l[1], l[2]) )
            self.q.append( l[3] )
            self.b.append( l[4] )
            self.e.append( l[5] )
            self.s.append( l[6] )

        for i in range(0, N):
            for j in range(0, N):
                a = coords[i]
                b = coords[j]

                d = ((a[0] - b[0])**2 + (a[1] - b[1])**2)**0.5

                self.matrix[i][j] = d



def isNumber(s):
    return s.isdigit or isFloat(s)

def isFloat(s):
    try:
        float(s)
    except ValueError:
        return False
    return True
def num(s):
    try:
        return int(s)
    except ValueError:
        return float(s)